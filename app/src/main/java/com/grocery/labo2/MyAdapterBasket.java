package com.grocery.labo2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MyAdapterBasket extends ArrayAdapter<Item> {
    ArrayList<Item> productList = new ArrayList<Item>();
    Context ctx;

    public MyAdapterBasket(Context context, int textViewResourceId, ArrayList<Item> objects){
        super(context, textViewResourceId, objects);
        productList = objects;
        this.ctx = context;
    }

    @Override
    public int getCount(){
        return super.getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.basket_item, null);

        ImageView imageView = (ImageView)v.findViewById(R.id.imageView);
        final Bitmap finalImage = productList.get(position).getImage();
        imageView.setImageBitmap(finalImage);

        TextView tvName = (TextView) v.findViewById(R.id.itemName);
        final String finalName = productList.get(position).getName();
        tvName.setText(finalName);

        TextView tvPrice = (TextView) v.findViewById(R.id.itemPrice);
        final String finalPrice = productList.get(position).getPrice();
        tvPrice.setText(finalPrice);

        TextView tvPricePerkg = (TextView) v.findViewById(R.id.itemPricePerKG);
        final String finalPricePerkg = productList.get(position).getPricePerKg();
        tvPricePerkg.setText(finalPricePerkg);

        final TextView qte = (TextView)v.findViewById(R.id.qtechosen);
        Button removeFromBasket = (Button)v.findViewById(R.id.btnDeleteFromBasket);

        removeFromBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                String btnText = b.getText().toString();

                AlertDialog alert = new android.app.AlertDialog.Builder(ctx).create();
                alert.setCancelable(false);
                alert.setTitle("REMOVE FROM BASKET");
                alert.setMessage("Are you sure you want to delete this item?");

                alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                        }
                );

                alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BasketArray.removeFromBasket(position);

                        Intent i = new Intent(ctx, BasketItems.class);
                        ctx.startActivity(i);
                        //BasketItems.finish();
                    }
                });
                alert.show();
            }
        });

        final TextView qte2 = (TextView)v.findViewById(R.id.qtechosen2);
        qte2.setText(String.valueOf(BasketArray.getBasketItems().get(position).getAmount()));
        Button btnadd = (Button)v.findViewById(R.id.btnadd2);
        Button btnsub = (Button)v.findViewById(R.id.btnsub2);
        Button addToBasket = (Button)v.findViewById(R.id.btnAddToBasket);

        btnadd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String val = qte2.getText().toString();
                val = String.valueOf(Integer.parseInt(val) + 1);
                qte2.setText(val);
            }
        });

        btnsub.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String val = qte2.getText().toString();

                if(Integer.parseInt(val) != 1){
                    val = String.valueOf(Integer.parseInt(val) - 1);
                    qte2.setText(val);
                }
            }
        });

        Button btnUpdate = (Button)v.findViewById(R.id.btnUpdateBasket);
        btnUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                BasketArray.updateQte(position, Integer.parseInt(qte2.getText().toString()));
                Toast toast = Toast.makeText(ctx, "Quantity updated", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        return v;
    }
}
