package com.grocery.labo2;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<Item> {
    ArrayList<Item> productList = new ArrayList<Item>();
    Context ctx;

    public MyAdapter(Context context, int textViewResourceId, ArrayList<Item> objects){
        super(context, textViewResourceId, objects);
        productList = objects;
        this.ctx = context;
    }

    @Override
    public int getCount(){
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_view_items, null);

        ImageView imageView = (ImageView)v.findViewById(R.id.imageView);
        final Bitmap finalImage = productList.get(position).getImage();
        imageView.setImageBitmap(finalImage);

        TextView tvName = (TextView) v.findViewById(R.id.itemName);
        final String finalName = productList.get(position).getName();
        tvName.setText(finalName);

        TextView tvPrice = (TextView) v.findViewById(R.id.itemPrice);
        final String finalPrice = productList.get(position).getPrice();
        tvPrice.setText(finalPrice);

        TextView tvPricePerkg = (TextView) v.findViewById(R.id.itemPricePerKG);
        final String finalPricePerkg = productList.get(position).getPricePerKg();
        tvPricePerkg.setText(finalPricePerkg);

        final TextView qte = (TextView)v.findViewById(R.id.qtechosen);
        Button btnadd = (Button)v.findViewById(R.id.btnadd);
        Button btnsub = (Button)v.findViewById(R.id.btnsub);
        Button addToBasket = (Button)v.findViewById(R.id.btnAddToBasket);

        btnadd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String val = qte.getText().toString();
                val = String.valueOf(Integer.parseInt(val) + 1);
                qte.setText(val);
            }
        });

        btnsub.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String val = qte.getText().toString();

                if(Integer.parseInt(val) != 1){
                    val = String.valueOf(Integer.parseInt(val) - 1);
                    qte.setText(val);
                }
            }
        });

        addToBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                String btnText = b.getText().toString();

                int val = Integer.parseInt(qte.getText().toString());

                if(val == 0){
                    Toast toast = Toast.makeText(ctx, "No quantity selected", Toast.LENGTH_SHORT);
                    toast.show();
                }

                else{
                    boolean itemExist = false;
                    int newqty = 0;

                    for(Item item: BasketArray.getBasketItems()){
                        if(item.getName() == finalName){
                            newqty = BasketArray.updateBasketItemQty(item, val);
                            itemExist = true;
                        }
                    }

                    if(itemExist){
                        String nameItem = finalName;
                        if(finalName.contains("(")){
                            nameItem = finalName.substring(0, finalName.indexOf("("));
                        }
                        Toast toast = Toast.makeText(ctx, "You now have " + newqty + " " + nameItem + " in your basket", Toast.LENGTH_SHORT);
                        toast.show();
                        qte.setText("1");
                    }

                    else{
                        Item item = new Item(finalImage, finalName, finalPrice, finalPricePerkg, val);
                        BasketArray.addToBasket(item);
                        qte.setText("1");

                        Toast toast;
                        if(finalName.contains("(")){ toast = Toast.makeText(ctx, finalName.substring(0, finalName.indexOf("(")) + " has been added to basket", Toast.LENGTH_SHORT); }

                        else{ toast = Toast.makeText(ctx, finalName + " has been added to yout basket", Toast.LENGTH_SHORT); }

                        toast.show();
                    }
                }
            }
        });
        return v;
    }
}
