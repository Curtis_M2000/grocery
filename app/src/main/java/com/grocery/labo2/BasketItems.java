package com.grocery.labo2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class BasketItems extends AppCompatActivity {
    static GridView simpleItemList;
    ArrayList<Item> searchList = new ArrayList<>();
    MyAdapterBasket myAdapter;
    GridView myGrid;
    TextView subtotal;
    TextView tax;
    TextView total;

    private static DecimalFormat df2 = new DecimalFormat("#.##");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_basket_items);

        subtotal = (TextView) findViewById(R.id.subtotal);
        tax = (TextView) findViewById(R.id.taxes);
        total = (TextView) findViewById(R.id.total);

        simpleItemList = (GridView)findViewById(R.id.myGrid2);
        myAdapter = new MyAdapterBasket(this,R.layout.basket_item,BasketArray.getBasketItems());
        simpleItemList.setAdapter(myAdapter);

        final EditText searchText = (EditText)findViewById(R.id.searchbar);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredText = searchText.getText().toString();
                search(enteredText);
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_LEFT = 0;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchText.getRight() - searchText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        searchText.setText("");
                        myAdapter = new MyAdapterBasket(BasketItems.this,R.layout.basket_item,BasketArray.getBasketItems());
                        simpleItemList.setAdapter(myAdapter);
                        return true;
                    }

                    else if((event.getRawX()+searchText.getPaddingLeft()) <= (searchText.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width()+searchText.getLeft())){
                        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(FruitsAndVegetables.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

                        Toast toast = Toast.makeText(BasketItems.this, String.valueOf(searchList.size()) + " items found", Toast.LENGTH_SHORT);
                        toast.show();
                        return true;
                    }
                }
                return false;
            }
        });

        calculateTotal();
    }

    public void search(String val){
        searchList.clear();

        for(Item item :BasketArray.getBasketItems()){
            if(item.getName().toUpperCase().contains(val.toUpperCase())){
                searchList.add(item);
            }
        }
        myAdapter = new MyAdapterBasket(this,R.layout.basket_item,searchList);
        simpleItemList.setAdapter(myAdapter);
    }

    public void calculateTotal(){
        double price = BasketArray.calculateTotal();
        subtotal.setText("SubTotal = $" + df2.format(price));
        tax.setText("Tax = $" + df2.format(price * 0.15));
        total.setText("Total = $" + df2.format(price * 1.15));
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, FruitsAndVegetables.class);
        startActivity(i);
        this.finish();
    }
}
