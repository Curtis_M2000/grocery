package com.grocery.labo2;

import java.util.ArrayList;

public class BasketArray {
    private static ArrayList<Item> basket = new ArrayList<>();

    public static String countItems(){
        return String.valueOf(basket.size());
    }

    public static void addToBasket(Item item){
        basket.add(item);
    }

    public static void removeFromBasket(int i){
        basket.remove(i);
    }

    public static ArrayList<Item> getBasketItems(){
        return basket;
    }

    public static int updateBasketItemQty(Item item, int qty){
        int itemIndex = basket.indexOf(item);
        Item newItem = item;
        newItem.setAmount(item.getAmount() + qty);
        basket.set(itemIndex, newItem);
        return newItem.getAmount();
    }

    public static double calculateTotal(){
        double total = 0.0;
        String val;

        for(Item item : basket){
            val = item.getPrice();

            if(val.contains(" ")){ val = val.substring(0, val.indexOf(" ")); }

            val = val.substring(1, val.length());
            total += Double.valueOf(val) * item.getAmount();
        }
        return total;
    }

    public static void updateQte(int pos, int qte){
        basket.get(pos).setAmount(qte);
    }
}
