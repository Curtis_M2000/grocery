package com.grocery.labo2;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class FruitsAndVegetables extends AppCompatActivity {
    GridView simpleItemList;
    ArrayList<Item> productList = new ArrayList<>();
    ArrayList<Item> searchList = new ArrayList<>();
    MyAdapter myAdapter;

    public String title = "FRUITS AND VEGETABLES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruits_and_vegetables);
        setTitle("FRUITS AND VEGETABLES");

        final EditText searchText = (EditText)findViewById(R.id.searchbar);

        simpleItemList = (GridView)findViewById(R.id.myGrid);
        getSupportActionBar().setElevation(0);

        loadFirstPage();

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredText = searchText.getText().toString();
                search(enteredText);
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_LEFT = 0;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchText.getRight() - searchText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        searchText.setText("");
                        myAdapter = new MyAdapter(FruitsAndVegetables.this,R.layout.list_view_items,productList);
                        simpleItemList.setAdapter(myAdapter);
                        return true;
                    }

                    else if((event.getRawX()+searchText.getPaddingLeft()) <= (searchText.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width()+searchText.getLeft())){
                        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(FruitsAndVegetables.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

                        Toast toast = Toast.makeText(FruitsAndVegetables.this, String.valueOf(searchList.size()) + " items found", Toast.LENGTH_SHORT);
                        toast.show();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public class bgWorker extends AsyncTask {
        public Context ctx;
        StringBuffer sb = null;

        ProgressDialog progress;

        public String malek;

        public String name = "No name";
        public String price = "No price";
        public String priceperkg = "No price per kg";
        public String urlimage = "";
        public String promo = "";
        public String normalPrice = "";
        String weight = "";
        public int amount = 0;

        public bgWorker(Context ctx){ this.ctx = ctx; }

        @Override
        public void onPreExecute(){
            productList.clear();
            myAdapter = new MyAdapter(ctx,R.layout.list_view_items,productList);
            simpleItemList.setAdapter(myAdapter);

            progress = new ProgressDialog(ctx);
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.setCancelable(false);
            progress.setIndeterminate(false);
            progress.setMessage("Loading " + title + "...");
            progress.setProgress(0);
            progress.show();
        }

        @Override
        public String doInBackground(Object[] param){
            String url = (String)param[0];
            String url2 = (String)param[0];
            int numPages = /*(int)param[1]*/3;

            try {
                for(int j = 0; j < numPages ; j++){
                    URL obj = new URL(url);
                    HttpURLConnection http = (HttpURLConnection)obj.openConnection();
                    http.setRequestMethod("GET");
                    http.addRequestProperty("User-Agent", "Mozilla");

                    BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()));
                    String input;
                    sb = new StringBuffer();

                    while((input = br.readLine()) != null){
                        sb.append(input + "\n");

                        if(input.contains("<img onerror=\"replaceErrorImageByDefaultImage(this)") && input.contains("src=\"https://product-images.metro.ca/images/")){ setImage(input); }

                        else if(input.contains("<div class=\"pt-title")){ setName(input); }

                        else if(input.contains("<span class=\"pi-price price-update pi-price-promo\">") && input.contains("&nbsp;/&nbsp;")){ setPromo(input);}

                        else if(input.contains("<span class=\"pi-price price-update")){ setPrice(input);}

                        else if(input.contains("<span class=\"pi-unit\">") || input.contains("<span class=\"pi-unit pi-price-promo\">")){ setPriceEnd(input);}

                        else if(input.contains("<span class=\"pi-price\">") && priceperkg != ""){ setNormalPrice(input);}

                        else if(input.contains("<span class=\"pi-price\">") && input.contains("<abbr title=\"")){ setPriceUnit(input);}

                        else if(input.contains("<span class=\"pt-weight\">")){ setWeight(input);}

                        else if(input.contains("<div class=\"tile-product__bottom-section__add-cart\">")){addToList();}
                    }
                    br.close();
                    url = url2 + "-page-" + (j+2);
                    progress.setProgress((((j+1) * 100)/numPages));
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Object o){
            myAdapter = new MyAdapter(ctx,R.layout.list_view_items,productList);
            simpleItemList.setAdapter(myAdapter);
            progress.cancel();
        }

        public Bitmap getBitMap(String imgUrl) {
            try {
                URL url = new URL(imgUrl);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                return null;
            }
        }

        public void setImage(String input){
            String [] words = input.split(" ");

            for(int i = 0; i < words.length; i++){
                if(words[i].contains("src=")){
                    urlimage = words[i];
                    urlimage = urlimage.substring(urlimage.indexOf("\"") + 1, urlimage.lastIndexOf("\""));
                }
            }
        }

        public void setName(String input){
            name = input;
            name = name.substring(0, name.indexOf("</div>"));
            name = name.substring(name.indexOf(">")+1, name.length());
            if(name.contains("&amp;")){ name = name.substring(0, name.indexOf("&")+1) + name.substring(name.indexOf(";")+1, name.length()); }
        }

        public void setPrice(String input){
            price = input;
            price = price.substring(0, price.indexOf("</span>"));
            price = price.substring(price.indexOf(">")+1, price.length());
        }

        public void setPriceEnd(String input){
            if(input.contains("avg") && !price.contains("avg.")){ price = price + " avg.";}
            if(input.contains("ea") && !price.contains("ea.")){ price = price + " ea.";}
        }

        public void setPriceUnit(String input){
            priceperkg = input;
            priceperkg = priceperkg.substring(priceperkg.indexOf(">"), priceperkg.indexOf(".")+3);
            String unit = " /unit";

            if(input.contains(">kg")){unit = " /kg";}
            else if(input.contains(">un")){unit = " /un";}
            else if(input.contains(">ml")){unit = " /100ml";}
            else if(input.contains(">sheets")){unit = " /100sheets";}
            else if(input.contains(">g")){unit = " /100g";}
            else if(input.contains(">m")){unit = " /m";}

            priceperkg = priceperkg.substring(priceperkg.indexOf("$"), priceperkg.length());
            priceperkg += unit;
        }

        public void setPromo(String input){
            promo = input;
            promo = promo.substring(0, promo.indexOf("&"));
            promo = promo.substring(promo.lastIndexOf(">")+1, promo.length());
            promo = promo + " / ";
        }

        public void setNormalPrice(String input){
            normalPrice = input;
            normalPrice = normalPrice.substring(0, normalPrice.indexOf("</span>"));
            normalPrice = normalPrice.substring(normalPrice.indexOf(">")+1, normalPrice.length());
        }

        public void setWeight(String input){
            weight = input;
            weight = weight.substring(weight.indexOf(">") + 1, weight.length());
            weight = weight.substring(0, weight.indexOf("<"));

            if(weight.length() > 0){ name = name + " (" + weight + ")"; }
        }

        public void addToList(){
            if(name != "" && price != "" && priceperkg != ""){
                if(promo != ""){ price = promo + price + " (" + normalPrice + ")"; }

                if(urlimage == ""){ urlimage = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSh5SgsLaoBqkuAAmQi49QHOq7EgiGqKknY4YiPq2GGYcWPNZtR";}

                productList.add(new Item(getBitMap(urlimage), name, price,priceperkg, amount));
                urlimage = name = price = priceperkg = promo = normalPrice = weight = "";
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        MenuInflater minf = getMenuInflater();
        minf.inflate(R.menu.navmenu, menu);

        SubMenu subMenu = menu.addSubMenu(Menu.NONE, Menu.FIRST, 1,"ALL CATEGORIES");
        subMenu.add(Menu.NONE, Menu.FIRST + 1, 0,"FRUITS AND VEGETABLES");
        subMenu.add(Menu.NONE, Menu.FIRST + 2, 1,"DAIRY AND EGGS");
        subMenu.add(Menu.NONE, Menu.FIRST + 3, 2,"PANTRY");
        subMenu.add(Menu.NONE, Menu.FIRST + 4, 3,"BEVERAGES");
        subMenu.add(Menu.NONE, Menu.FIRST + 5, 4,"BEER AND WIN");
        subMenu.add(Menu.NONE, Menu.FIRST + 6, 5,"MEAT AND  POULTRY");
        subMenu.add(Menu.NONE, Menu.FIRST + 7, 6,"VEGAN AND VEGETARIAN");
        subMenu.add(Menu.NONE, Menu.FIRST + 8, 7,"ORGANIC GROCERIE");
        subMenu.add(Menu.NONE, Menu.FIRST + 9, 8,"SNACKS");
        subMenu.add(Menu.NONE, Menu.FIRST + 10, 9,"FROZEN");
        subMenu.add(Menu.NONE, Menu.FIRST + 11, 10,"BREAD AND BAKERY PRODUCTS");
        subMenu.add(Menu.NONE, Menu.FIRST + 12, 11,"DELI AND PREPARED MEALS");
        subMenu.add(Menu.NONE, Menu.FIRST + 13, 12,"FISH AND SEA FOOD");
        subMenu.add(Menu.NONE, Menu.FIRST + 14, 13,"WORLD CUISINE");
        subMenu.add(Menu.NONE, Menu.FIRST + 15, 14,"HOUSEHOLD AND BEAUTY");
        subMenu.add(Menu.NONE, Menu.FIRST + 16, 15,"PET CARE");
        subMenu.add(Menu.NONE, Menu.FIRST + 17, 16,"PHARMACY");
        subMenu.add(Menu.NONE, Menu.FIRST + 18, 17,"NATURE'S SIGNATURE");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        super.onOptionsItemSelected(item);

        change(item.getItemId());

        return true;
    }

    public void search(String val){
        searchList.clear();

        for(Item item :productList){
            if(item.getName().toUpperCase().contains(val.toUpperCase())){
                searchList.add(item);
            }
        }
        myAdapter = new MyAdapter(this,R.layout.list_view_items,searchList);
        simpleItemList.setAdapter(myAdapter);
    }

    @Override
    public void onBackPressed() {
        AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
        alert.setCancelable(false);
        alert.setTitle("Exit");
        alert.setMessage("Are you sure you want to exit");

        alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                }
        );

        alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        alert.show();
    }

    public void loadFirstPage(){
        if(connected()){
            bgWorker bg = new bgWorker(this);
            bg.execute("https://www.metro.ca/en/online-grocery/aisles/fruits-vegetables", 27);
        }
        else{
            AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
            alert.setCancelable(false);
            alert.setTitle("Connection Error");
            alert.setMessage("You seem to have no Internet connection!");

            alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "EXIT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    }
            );

            alert.setButton(AlertDialog.BUTTON_POSITIVE, "TRY AGAIN", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    loadFirstPage();
                }
            });
            alert.show();
        }
    }

    public void change(final int id){
        if(connected()){
            bgWorker bg = new bgWorker(FruitsAndVegetables.this);

            switch (id){
                case Menu.FIRST + 1:
                    title = "FRUITS AND VEGETABLES";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/fruits-vegetables", 27);
                    break;

                case Menu.FIRST + 2:
                    title = "DAIRY AND EGGS";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/dairy-eggs", 50);
                    break;

                case Menu.FIRST + 3:
                    title = "PANTRY";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/pantry", 165);
                    break;

                case Menu.FIRST + 4:
                    title = "BEVERAGES";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/beverages", 74);
                    break;

                case Menu.FIRST + 5:
                    title = "BEER AND WIN";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/beer-wine", 2);
                    break;

                case Menu.FIRST + 6:
                    title = "MEAT AND  POULTRY";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/meat-poultry", 26);
                    break;

                case Menu.FIRST + 7:
                    title = "VEGAN AND VEGETARIAN";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/vegan-vegetarian-food", 24);
                    break;

                case Menu.FIRST + 8:
                    title = "ORGANIC GROCERIES";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/organic-groceries", 45);
                    break;

                case Menu.FIRST + 9:
                    title = "SNACKS";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/snacks", 81);
                    break;

                case Menu.FIRST + 10:
                    title = "FROZEN";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/frozen", 61);
                    break;

                case Menu.FIRST + 11:
                    title = "BREAD AND BAKERY PRODUCTS";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/bread-bakery-products", 30);
                    break;

                case Menu.FIRST + 12:
                    title = "DELI AND PREPARED MEALS";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/deli-prepared-meals", 31);
                    break;

                case Menu.FIRST + 13:
                    title = "FISH AND SEA FOOD";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/fish-seafood", 12);
                    break;

                case Menu.FIRST + 14:
                    title = "WORLD CUISINE";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/world-cuisine", 13);
                    break;

                case Menu.FIRST + 15:
                    title = "HOUSEHOLD AND BEAUTY";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/household-cleaning", 41);
                    break;

                case Menu.FIRST + 16:
                    title = "PET CARE";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/pet-care", 15);
                    break;

                case Menu.FIRST + 17:
                    title = "PHARMACY";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/pharmacy", 75);
                    break;

                case Menu.FIRST + 18:
                    title = "NATURE'S SIGNATURE";
                    setTitle(title);
                    bg.execute("https://www.metro.ca/en/online-grocery/aisles/nature-s-signature", 109);
                    break;

                case R.id.basket:
                    Intent i = new Intent(this, BasketItems.class);
                    startActivity(i);
                    break;

                case R.id.close:
                    AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
                    alert.setCancelable(false);
                    alert.setTitle("Exit");
                    alert.setMessage("Are you sure you want to exit?");

                    alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }
                    );

                    alert.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    });
                    alert.show();
                    break;
            }
        }
        else{
            AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
            alert.setCancelable(false);
            alert.setTitle("Connection Error");
            alert.setMessage("You seem to have no Internet connection!");

            alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "EXIT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    }
            );

            alert.setButton(AlertDialog.BUTTON_POSITIVE, "TRY AGAIN", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    change(id);
                }
            });
            alert.show();
        }
    }

    private boolean connected(){
        ConnectivityManager connectivityManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo !=null && activeNetworkInfo.isConnected();
    }
}
