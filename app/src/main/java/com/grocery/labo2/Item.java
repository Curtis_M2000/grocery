package com.grocery.labo2;

import android.graphics.Bitmap;

public class Item {
    private Bitmap image;
    private String name;
    private String price;
    private String pricePerKg;
    private int amount;

    public Item(Bitmap image, String name, String price, String pricePerKg, int amount){
        this.image = image;
        this.name = name;
        this.price = price;
        this.pricePerKg = pricePerKg;
        this.amount = amount;
    }

    public Bitmap getImage(){ return this.image; }
    public String getName(){return this.name;}
    public String getPrice(){return this.price;}
    public String getPricePerKg() { return pricePerKg; }
    public int getAmount(){return this.amount;}
    public void setAmount(int amount){this.amount = amount;}
}
